import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Input from "../../components/uielements/input";
import Button from "../../components/uielements/button";
import authAction from "../../redux/auth/actions";
import IntlMessages from "../../components/utility/intlMessages";
import SignInStyleWrapper from "./signin.style";

const { login, jwtLogin } = authAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
    username: "",
    password: ""
  };

  constructor(props, context) {
    super(props, context);
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleJWTLogin = () => {
    const { jwtLogin, history } = this.props;
    const { username, password } = this.state;
    console.log('ssss');
    const userInfo = {
      username: username,
      password: password
    };
    jwtLogin(history, userInfo);
  };

  handleLogin = () => {
    const { login } = this.props;
    const { username, password } = this.state;

    localStorage.setItem("username", username);
    localStorage.setItem("password", password);

    //this.onLogin();
    //onLogin()

    login();
    this.props.history.push("/dashboard");
  };
  handleUsernameChange(event) {
    console.log('do handleUsernameChange');
    this.setState({ username: event.target.value });
  }
  handlePasswordChange(event) {
    console.log('do handlePasswordChange');
    this.setState({ password: event.target.value });
  }
  _handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      console.log('do validate');
      this.handleLogin()
    }
  }
  
  render() {
    const from = { pathname: "/dashboard" };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="login-background">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div style={{ justifyContent: "center", display: "flex"}}>
              <img
                alt="#"
                src={require("../../image/logo.svg")}
                style={{ height: 137, width: 120, marginBottom: 14 }}
              />
            </div>
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <p style={{ textAlign: "center" }}>ADMIN</p>
              </Link>
            </div>

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input
                  id="inputUserName"
                  size="large"
                  name="username"
                  placeholder="Username"
                  value={this.state.username}
                  onChange={this.handleUsernameChange.bind(this)}
                  onKeyDown={event => {
                    if (event.key === 'Enter') {
                      console.log('do validate');
                      this.handleJWTLogin()
                    }
                  }}
                />
              </div>

              <div className="isoInputWrapper">
                <Input
                  id="inpuPassword"
                  size="large"
                  name="password"
                  type="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.handlePasswordChange.bind(this)}
                  onKeyDown={event => {
                    if (event.key === 'Enter') {
                      console.log('do validate');
                      this.handleJWTLogin()
                    }
                  }}
                />
              </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                
                <Button type="primary" onClick={this.handleJWTLogin} block>
                  <IntlMessages id="page.signInButton" />
                </Button>
              </div>
              
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false
  }),
  { login, jwtLogin }
)(SignIn);
